//
//  Configuration.swift
//  Hermes
//
//  Created by Daniel Novio on 11/07/16.
//  Copyright © 2016 Daniel Novio. All rights reserved.
//

public class Configuration {
    
    public var baseURL: String! {
        let configuration = configurationFile()
        
        #if RELEASE
            guard let url = configuration!["baseURL"] else {
                NSLog("Ooops! Didn't find baseURL on configuration file")
                
                return ""
            }
        #else
            guard let url = configuration!["debugURL"] else {
                NSLog("Ooops! Didn't find debugURL on configuration file")
                
                guard let url = configuration!["baseURL"] else {
                    NSLog("Ooops! Didn't find neither debugURL and baseURL on configuration file")
                    
                    return ""
                }
                
                return url as! String
            }
        #endif
        
        return url as! String
    }
    
    public var contentType: String! {
        let configuration = configurationFile()
        
        guard let content = configuration!["contentType"] else {
            NSLog("Didn't find contentType on configuration file")
            
            return ""
        }
        
        return content as! String
    }
    
    public var headers: [String : String]? {
        let configuration = configurationFile()
        
        guard let headersDict = configuration!["headers"] else {
            NSLog("Didn't find headers on configuration file. Overwrite to use it!")
            
            return nil
        }
        
        return headersDict as? [String : String]
    }
    
    
    func configurationFile() -> NSDictionary? {
        guard let path = NSBundle.mainBundle().pathForResource("Configuration", ofType: "plist") else {
            NSLog("Didn't find configuration file. Configuration variables need to be overwrited")
            
            return nil
        }
        
        let configuration = NSDictionary(contentsOfFile: path)
        
        return configuration
    }
}
