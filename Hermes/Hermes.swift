//
//  Hermes.swift
//  Hermes
//
//  Created by Daniel Novio on 21/06/16.
//  Copyright © 2016 Daniel Novio. All rights reserved.
//

import Foundation

public func receiveObjects<T where T:ResponseObjectSerializable, T:ResponseCollectionSerializable>(path: String!, parameters: [String : AnyObject]?,  sucess: (T?, [T]?) -> Void, failure: String -> Void) {
    Client.instance.getObjects(path, parameters: parameters, sucess: sucess, failure: failure)
}

public func receiveJSON(path: String!,parameters: [String : AnyObject]?, sucess: (AnyObject) -> Void, failure: (String) -> Void) {
    Client.instance.getJSON(path, parameters: parameters, sucess: sucess, failure: failure)
}

public func deliverJSON(path: String!, parameters: [String : AnyObject]?, sucess: (AnyObject) -> Void, failure: (String) -> Void) {
    Client.instance.postJSON(path, parameters: parameters, sucess: sucess, failure: failure)
}

public func overrideConfigurationHeaders(headers: [String : String]) {
    Client.instance.headers = headers
}