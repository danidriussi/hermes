//
//  Hermes.h
//  Hermes
//
//  Created by Daniel Novio on 21/06/16.
//  Copyright © 2016 Daniel Novio. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for Hermes.
FOUNDATION_EXPORT double HermesVersionNumber;

//! Project version string for Hermes.
FOUNDATION_EXPORT const unsigned char HermesVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <Hermes/PublicHeader.h>


