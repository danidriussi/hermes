//
//  Client.swift
//  Hermes
//
//  Created by Daniel Novio on 21/06/16.
//  Copyright © 2016 Daniel Novio. All rights reserved.
//

import Foundation

import Alamofire

public enum BackendError: ErrorType {
    case Network(error: NSError)
    case DataSerialization(reason: String)
    case JSONSerialization(error: NSError)
    case ObjectSerialization(reason: String)
    case XMLSerialization(error: NSError)
}

public protocol ResponseObjectSerializable {
    init?(response: NSHTTPURLResponse, representation: AnyObject)
}

public protocol ResponseCollectionSerializable {
    static func collection(response response: NSHTTPURLResponse, representation: AnyObject) -> [Self]
}

public class Client: NSObject {
    
    static let instance = Client()
    
    let configuration = Configuration()
    
    final var baseUrl: String {
        return configuration.baseURL
    }
    
    final var theContentType: String {
        return configuration.contentType
    }
    
    private var defaultHeaders: [String : String]? {
        get {
            return configuration.headers
        }
        set {
        }
    }
    
    var headers: [String : String]? {
        get {
            return defaultHeaders
        }
        set(value) {
            defaultHeaders = value
        }
    }
    
    func getObjects<T where T:ResponseObjectSerializable, T:ResponseCollectionSerializable>(path: String!, parameters: [String : AnyObject]?, sucess: (T?, [T]?) -> Void, failure: String -> Void) {
        
        let url = baseUrl + path
        
        Alamofire.request(.GET, url, parameters: parameters, encoding: .JSON, headers: headers)
            .validate(statusCode: 200..<300)
            .validate(contentType: [theContentType])
            .responseObject { (response: Response<T, BackendError>) -> Void in
                if response.result.isSuccess {
                    guard let responseObject = response.result.value else {
                        NSLog("Nil response object")
                        
                        return
                    }
                    sucess(responseObject, nil)
                    
                } else {
                    failure(response.debugDescription)
                }
            }
            .responseCollection { (response: Response<[T], BackendError>) -> Void in
                if response.result.isSuccess {
                    guard let responseObjects = response.result.value else {
                        NSLog("Empty results array")
                        
                        return
                    }
                    
                    sucess(nil, responseObjects)
                    
                } else {
                    failure(response.debugDescription)
                }
        }
    }
    
    func getJSON(path: String!, parameters: [String : AnyObject]?, sucess: (AnyObject) -> Void, failure: (String) -> Void) {
        let url = baseUrl + path
        
        Alamofire.request(.GET, url, parameters: parameters, encoding: .JSON, headers: headers)
            .validate(statusCode: 200..<300)
            .validate(contentType: [theContentType])
            .responseJSON { (response) in
                
                if response.result.isSuccess {
                    guard let json = response.result.value else {
                        NSLog("Problem. Sucess but no json.")
                        
                        return
                    }
                    
                    sucess(json)
                    
                } else {
                    failure(response.result.debugDescription) //Might need to change it
                }
        }
    }
    
    func postJSON(path: String!, parameters: [String : AnyObject]?, sucess: (AnyObject) -> Void, failure: (String) -> Void) {
        let url = baseUrl + path
        
        Alamofire.request(.POST, url, parameters: parameters, encoding: .JSON, headers: headers)
            .validate()
            .responseJSON { (response) in
                if response.result.isSuccess {
                    guard let json = response.result.value else {
                        NSLog("Problem with post return value")
                        
                        return
                    }
                    
                    sucess(json)
                    
                } else {
                    failure(response.result.debugDescription)
                }
        }
    }
}

public extension Request {
    public func responseObject<T: ResponseObjectSerializable>(completionHandler: Response<T, BackendError> -> Void) -> Self {
        let responseSerializer = ResponseSerializer<T, BackendError> { request, response, data, error in
            guard error == nil else {
                return .Failure(.Network(error: error!))
            }
            
            let JSONResponseSerializer = Request.JSONResponseSerializer(options: .AllowFragments)
            let result = JSONResponseSerializer.serializeResponse(request, response, data, error)
            
            switch result {
                
            case .Success(let value):
                if value is NSDictionary {
                    if let response = response, responseObject = T(response: response, representation: value) {
                        return .Success(responseObject)
                        
                    } else {
                        return .Failure(.ObjectSerialization(reason: "JSON could not be serialized into response object: \(value)"))
                    }
                    
                } else {
                    return .Failure(.ObjectSerialization(reason: "JSON could not be serialized into response object: \(value)"))
                }
                
            case .Failure(let error):
                return .Failure(.JSONSerialization(error: error))
            }
        }
        return response(responseSerializer: responseSerializer, completionHandler: completionHandler)
    }
    
    public func responseCollection<T: ResponseCollectionSerializable>(completionHandler: Response<[T], BackendError> -> Void) -> Self {
        let responseSerializer = ResponseSerializer<[T], BackendError> { request, response, data, error in
            guard error == nil else {
                return .Failure(.Network(error: error!))
            }
            
            let JSONSerializer = Request.JSONResponseSerializer(options: .AllowFragments)
            let result = JSONSerializer.serializeResponse(request, response, data, error)
            
            switch result {
                
            case .Success(let value):
                if let response = response {
                    return .Success(T.collection(response: response, representation: value))
                    
                } else {
                    return .Failure(. ObjectSerialization(reason: "Response collection could not be serialized due to nil response"))
                }
                
            case .Failure(let error):
                return .Failure(.JSONSerialization(error: error))
            }
        }
        return response(responseSerializer: responseSerializer, completionHandler: completionHandler)
    }
}

public extension ResponseCollectionSerializable where Self: ResponseObjectSerializable {
    static func collection(response response: NSHTTPURLResponse, representation: AnyObject) -> [Self] {
        var collection = [Self]()
        
        if let representation = representation as? [[String: AnyObject]] {
            for itemRepresentation in representation {
                if let item = Self(response: response, representation: itemRepresentation) {
                    collection.append(item)
                }
            }
        }
        
        return collection
    }
 }