Pod::Spec.new do |s|
  s.platform = 'ios'
  s.ios.deployment_target = '8.0'
  s.name     = 'Hermes'
  s.homepage = 'https://github.com/danidriussi/Hermes'
  s.version  = '0.0.1'
  s.license  = 'MIT'
  s.summary  = 'Post and receive everything you need with Hermes!'
  s.author  = {'Daniel Novio' => 'danidriussi@gmail.com'}
  s.source   = { :git => 'git@bitbucket.org:danidriussi/hermes.git', :tag => "#{s.version}"}
  s.source_files = 'Hermes/*'
  s.requires_arc = true
  s.dependency 'Alamofire', '~> 3.4.1'
end
